import numpy as np
import torch
import torchvision
import matplotlib.pyplot as plt
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
from GaborKernel5x5 import GaborKernel5x5
from GaborKernelMesh import GaborKernelMesh
from GaborKernel import GaborKernel
from cnn import CNNModel

kernel_size = np.array([28])
theta_ = np.arange(0, np.pi, np.pi / 4)
lambda_ = np.array([3, 5, 7])
sigma_ = np.array([1.0, 2.0, 3.0])
gamma_ = np.array([0.5, 1.25, 2.0])
psi_ = np.array([0, np.pi / 2])


def get_images(train_images, train_labels, number_of_samples=5421):
    images_by_digit = []
    labels_by_digit = []

    for digit in range(10):
        digit_images, digit_labels = get_images_of_digit(list(zip(train_images, train_labels)), digit,
                                                         number_of_samples)

        images_by_digit.append(digit_images)
        labels_by_digit.extend([digit] * number_of_samples)

    return images_by_digit, labels_by_digit


def get_images_of_digit(dataset, digit, count):
    images = []
    labels = []

    for i in range(len(dataset)):
        image, label = dataset[i]
        if label == digit:
            images.append(np.array(image))
            labels.append(label)
            if len(images) == count:
                break

    return images, labels



def runCNNWithNoise(model, sigmas):
    accuracies = model.run(sigmas)
    
    return accuracies

def runGaborWithNoiseAndMesh5x5(gabor_kernel5x5, NUM_SAMPLES_PER_DIGIT, NUMBER_OF_PROCESSES, TOTAL_CLUSTERS,
                                test_images, test_labels, SIGMAS,
                                images_by_digit, labels_by_digit):

    result = gabor_kernel5x5.run(NUM_SAMPLES_PER_DIGIT, NUMBER_OF_PROCESSES, TOTAL_CLUSTERS,
                                test_images, test_labels, SIGMAS,
                                images_by_digit, labels_by_digit)
    return result



def runGaborWithNoiseAndMesh(kernel, x_train, y_train, x_test, y_test, sigmas, samples_per_digit):
    accuracies = kernel.run(x_train, y_train, x_test, y_test, sigmas, samples_per_digit)
    return accuracies





def runGaborWithNoise(kernel, x_train, y_train, x_test, y_test, sigmas, samples_per_digit):
    accuracies = kernel.run(x_train, y_train, x_test, y_test, sigmas, samples_per_digit)
    return accuracies




def main():
    print("WELCOME TO THE GREATEST BATTLE OF ALL TIME")
    print("CNN VS GABOR VS GABOR WITH MESH 5X5")

    
    SIGMAS = [0.1 * i for i in range(0, 11)]
    NUM_SAMPLES_PER_DIGIT = 100
    NUMBER_OF_PROCESSES = 10
    TOTAL_CLUSTERS = [82, 98, 97, 99, 1, 1, 1, 1, 1, 1, 1, 4]
    TOTAL_TEST_IMAGES = 50

    mnist_dataset = tf.keras.datasets.mnist
    (train_images, train_labels), (test_images, test_labels) = mnist_dataset.load_data()
    images_by_digit, labels_by_digit = get_images(train_images, train_labels, NUM_SAMPLES_PER_DIGIT)

    model = CNNModel(epochs=1)
    gabor_kernel5x5 = GaborKernel5x5(kernel_size, theta_, lambda_, sigma_, gamma_, psi_)
    gabor_kernel_mesh = GaborKernelMesh(kernel_size, theta_, lambda_, sigma_, gamma_, psi_)
    gabor_kernel = GaborKernel(kernel_size, theta_, lambda_, sigma_, gamma_, psi_)

    test_images = test_images[:TOTAL_TEST_IMAGES]
    test_labels = test_labels[:TOTAL_TEST_IMAGES]
    images_by_digit = np.vstack(images_by_digit)
    labels_by_digit = np.array(labels_by_digit)

    cnn_accuracies = runCNNWithNoise(model, SIGMAS)
    gabor_simple_accuracies = runGaborWithNoise(gabor_kernel, images_by_digit, labels_by_digit, test_images, test_labels, SIGMAS, NUM_SAMPLES_PER_DIGIT)
    gabor_mesh_accuracies = runGaborWithNoiseAndMesh(gabor_kernel_mesh, images_by_digit, labels_by_digit, test_images, test_labels, SIGMAS, NUM_SAMPLES_PER_DIGIT)
    gabor_mesh_5x5_accuracies = runGaborWithNoiseAndMesh5x5(gabor_kernel5x5, NUM_SAMPLES_PER_DIGIT, NUMBER_OF_PROCESSES, TOTAL_CLUSTERS,
                                test_images, test_labels, SIGMAS,
                                images_by_digit, labels_by_digit)
    

    plt.plot(SIGMAS, cnn_accuracies, label="CNN", marker="o")
    plt.plot(SIGMAS, gabor_simple_accuracies, label="Gabor Simple", marker="o")
    plt.plot(SIGMAS, gabor_mesh_accuracies, label="Gabor Mesh", marker="o")
    plt.plot(SIGMAS, gabor_mesh_5x5_accuracies, label="Gabor Mesh 5x5", marker="o")
    plt.title("CNN vs Gabor-alternatives for increasing noise")
    plt.xlabel("Sigma")
    plt.grid()
    plt.xticks(SIGMAS)
    plt.yticks(np.arange(0, 1.1, 0.1))
    plt.ylabel("Accuracy")
    plt.legend()
    plt.show()


if __name__ == "__main__":
    main()
