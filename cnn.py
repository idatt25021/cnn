import numpy as np
from tensorflow import keras
from keras import layers

class CNNModel():
    def __init__(self, epochs):
        # Model / data parameters
        self.num_classes = 10
        self.input_shape = (28, 28, 1)
        self.batch_size = 128
        self.epochs = epochs
        self.model = self._build_model()

        # Load and preprocess data
        self.x_train, self.y_train, self.x_test, self.y_test = self._load_and_preprocess_data()

    def _load_and_preprocess_data(self):
        # Load the data
        (x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

        # Preprocess the data
        x_train = x_train.astype("float32") / 255
        x_test = x_test.astype("float32") / 255
        x_train = np.expand_dims(x_train, -1)
        x_test = np.expand_dims(x_test, -1)

        y_train = keras.utils.to_categorical(y_train, self.num_classes)
        y_test = keras.utils.to_categorical(y_test, self.num_classes)

        return x_train, y_train, x_test, y_test

    def _build_model(self):
        model = keras.Sequential(
            [
                keras.Input(shape=self.input_shape),
                layers.Conv2D(32, kernel_size=(3, 3), activation="relu"),
                layers.MaxPooling2D(pool_size=(2, 2)),
                layers.Conv2D(64, kernel_size=(3, 3), activation="relu"),
                layers.MaxPooling2D(pool_size=(2, 2)),
                layers.Flatten(),
                layers.Dropout(0.5),
                layers.Dense(self.num_classes, activation="softmax"),
            ]
        )
        return model

    def compile_and_train(self):
        self.model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
        self.model.fit(self.x_train, self.y_train, batch_size=self.batch_size, epochs=self.epochs, validation_split=0.1)

    def evaluate_with_noise(self, sigmas):
        accuracy = []
        for sigma in sigmas:
            x_test_noisy = self.add_gaussian_noise(self.x_test, sigma)
            score = self.model.evaluate(x_test_noisy, self.y_test, verbose=0)
            accuracy.append(score[1])
        return accuracy
    
    @staticmethod
    def add_gaussian_noise(images, sigma):
        if(sigma == 0):
            return images
        noisy_images = []
        for img in images:
            mean = 0
            std = sigma
            noise = np.random.normal(mean, std, img.shape)
            img += noise
            noisy_images.append(img)
        return np.clip(noisy_images, 0, 1)
  
    def run(self, sigmas):
      self.compile_and_train()
      return self.evaluate_with_noise(sigmas)

