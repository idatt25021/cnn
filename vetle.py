import cv2
import numpy as np
import torchvision
from sklearn import preprocessing
from sklearn.cluster import KMeans
import pandas as pd


NUMBERS = 1000
TEST_NUMBERS = 50
DIGITS = 10

mnist_data = torchvision.datasets.MNIST('./data', train=True, download=True)
train_mnist_data = torchvision.datasets.MNIST('./data', train=False, download=True)


def gabor_filteres():
    for i in range(2):
        print()

    filters = []
    num_filters = 8
    ksize = 28  # The local area to evaluate
    psi = 0  # Offset value - lower generates cleaner results
    for theta in np.arange(0, np.pi, np.pi / num_filters):
        for lambd in np.arange(1.0, 10.0, 2):  # Theta is the orientation for edge detection 4
            for gamma in np.arange(0.1, 1, 0.2):  # 6
                for sigma in np.arange(2.0, 10, 2):  # 5
                    kern = cv2.getGaborKernel((ksize, ksize), sigma, theta, lambd, gamma, psi, ktype=cv2.CV_64F)
                    kern /= 1.0 * kern.sum()  # Brightness normalization
                    filters.append(kern)
    return filters


def apply_filter(image, filters):
    # Sørg for at filteret er av samme størrelse som bildet
    features = []
    for filter_ in filters:  # Bruker en for-løkke direkte på listen

        # Sjekk at filteret og bildet er av samme størrelse
        if filter_.shape != image.shape:
            raise ValueError("Filteret og bildet må være av samme størrelse")

        # Utfør elementvis multiplikasjon og summer resultatene
        scalar = np.sum(np.multiply(image, filter_))
        features.append(scalar)

    return features


def apply_filter_to_images(images, filters):
    filtered_images = []
    for img in images:
        filtered_features = apply_filter(img, filters)
        if filtered_features is not None:  # Make sure the features are not None
            filtered_images.append(filtered_features)
    return np.array(filtered_images)


# New function to get a single image from MNIST dataset
def get_images_of_digit(dataset, digit, count):
    images = []
    labels = []

    for i in range(len(dataset)):
        image, label = dataset[i]
        if label == digit:
            images.append(np.array(image))
            labels.append(label)
            if len(images) == count:
                break

    return images, labels


def get_test_images_of_digit(dataset, count):
    images = []
    labels = []
    for j in range(len(dataset)):
        image, label = dataset[j]
        images.append(np.array(image))
        labels.append(label)
        if len(images) == count:
            break

    return images, labels


def padd_images(images):
    padded_images = []
    for image_array in images:
        padded = np.pad(image_array, ((0, 1), (0, 1)), 'constant', constant_values=0)
        padded_images.append(padded)
    return padded_images


# We create our gabor filters, and then apply them to our image
filters = gabor_filteres()
list_of_filtered_numbers = []

for i in range(DIGITS):
    images, labels = get_images_of_digit(mnist_data, i, NUMBERS)
    padded = padd_images(images)
    filtered_images = apply_filter_to_images(padded, filters)

    X = np.array(filtered_images)
    print(X.shape)
    y = np.array(labels)

    #data_standardized = preprocessing.scale(X)  # standariserer feturene
    list_of_filtered_numbers.append((X, y))


clusters = []

for i in range(DIGITS):
    data, labels = list_of_filtered_numbers[i]
    kmeans = KMeans(n_clusters=100, n_init=10)
    kmeans.fit(data)
    clusters.append((kmeans.cluster_centers_, i))

test_images, test_labels = get_test_images_of_digit(train_mnist_data, TEST_NUMBERS)
padd_test_images = padd_images(test_images)
filtered_test_image = apply_filter_to_images(padd_test_images, filters)

filtered_test_image = np.array(filtered_test_image)


def euklidisk_avstand(punkt1, punkt2):
    return np.sqrt(np.sum((punkt1 - punkt2) ** 2))


def find_closest_cluster(search_number):
    minste_avstand = float('inf')  # Sett til uendelig i starten
    naermeste_kluster_index = -1

    for i, kluster in enumerate(clusters):
        cluster_centers_, label = kluster
        for j, center in enumerate(cluster_centers_):
            avstand = euklidisk_avstand(search_number, center)
            if avstand < minste_avstand:
                minste_avstand = avstand
                naermeste_kluster_index = i

    return naermeste_kluster_index


predicted_numbers = []

for i in range(TEST_NUMBERS):
    predicted_numbers.append(find_closest_cluster(filtered_test_image[i]))


def nøyaktighet():
    riktig = 0
    for frank, predicted_number in enumerate(predicted_numbers):
        print(f"predikert verdi er : {predicted_number}, faktisk verdi er {test_labels[frank]}")
        if predicted_number == test_labels[frank]:
            riktig += 1
    print(f"Nøyktigheten er: {riktig / TEST_NUMBERS * 100}")


nøyaktighet()