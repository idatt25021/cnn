import numpy as np
import matplotlib.pyplot as plt
import itertools
import cv2

def get_gabor_filters():
    kernel_size = np.array([28])
    theta_ = np.arange(0, np.pi, np.pi / 4)
    lambda_ = np.array([3, 5, 7])
    sigma_ = np.array([1.0, 2.0, 3.0])
    gamma_ = np.array([0.5, 1.25, 2.0])
    psi_ = np.array([0, np.pi / 2])

    combinations = list(itertools.product(kernel_size, theta_, lambda_, sigma_, gamma_, psi_))

    gabor_kernels = [cv2.getGaborKernel((ks, ks), sigma, theta, lambd, gamma, psi, ktype=cv2.CV_32F) for
                     ks, theta, lambd, sigma, gamma, psi in combinations]

    return gabor_kernels

gabor_kernels = get_gabor_filters()


filter_size = 29
summed_fourier = np.zeros((filter_size, filter_size))

for gabor in gabor_kernels:
    # Compute the Fourier transform of the Gabor filter
    f = np.fft.fftshift(np.fft.fft2(gabor))
    magnitude = np.abs(f)
    summed_fourier += magnitude

plt.imshow(np.log(1 + summed_fourier), cmap='hot')
plt.title('Fourier Domain Coverage')
# make colorbar go from 0 to max value
plt.colorbar()
plt.clim(0)
plt.show()
