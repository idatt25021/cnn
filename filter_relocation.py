import numpy as np
import tensorflow as tf
import math
import matplotlib.pyplot as plt
import cv2
import itertools
from sklearn.cluster import KMeans

def get_images_of_digit(dataset, digit, count):
    images = []
    labels = []

    for i in range(len(dataset)):
        image, label = dataset[i]
        if label == digit:
            images.append(np.array(image))
            labels.append(label)
            if len(images) == count:
                break

    return images, labels

def get_gabor_filters():
    kernel_size = np.array([28])
    theta_ = np.arange(0, np.pi, np.pi / 4)
    lambda_ = np.array([3, 5, 7])
    sigma_ = np.array([1.0, 2.0, 3.0])
    gamma_ = np.array([0.5, 1.25, 2.0])
    psi_ = np.array([0, np.pi / 2])

    combinations = list(itertools.product(kernel_size, theta_, lambda_, sigma_, gamma_, psi_))

    gabor_kernels = [cv2.getGaborKernel((ks, ks), sigma, theta, lambd, gamma, psi, ktype=cv2.CV_32F) for
                     ks, theta, lambd, sigma, gamma, psi in combinations]

    return gabor_kernels

def apply_filter(images, kernels):
    kernel_size = kernels[0].shape
    norm_kernels = [np.linalg.norm(k.flatten()) for k in kernels]

    filtered_images = []
    for img in images:

        img_float = img.astype(np.float32) / 255.0
        img_size = img_float.shape

        if kernel_size != img_size:
            padding = (
                (0, kernel_size[0] - img_size[0]),
                (0, kernel_size[1] - img_size[1])
            )
            img_float_padded = np.pad(img_float, padding, mode='constant', constant_values=0)
        else:
            img_float_padded = img_float

        if kernels[0].shape != img_float_padded.shape:
            raise ValueError("Kernel and image must have the same dimensions after padding")

        image_as_vector = img_float_padded.flatten()
        norm_image = np.linalg.norm(image_as_vector)

        filters_applied = []

        for kernel, norm_kernel in zip(kernels, norm_kernels):

            kernel_as_vector = kernel.flatten()
            dot_product = np.dot(image_as_vector, kernel_as_vector)
            cos_of_theta = dot_product / (norm_image * norm_kernel)

            filters_applied.append(cos_of_theta)

        filtered_images.append(filters_applied)

    return filtered_images


def kmeans_clustering(filtered_images, num_clusters):
    kmeans = KMeans(n_clusters=num_clusters, random_state=42, n_init='auto').fit(filtered_images)
    return kmeans


def plot_images(original_images, filtered_images, kernels, num_filters=5):
    for idx, (original, filtered_set) in enumerate(zip(original_images, filtered_images)):
        plt.figure(figsize=(15, 3))
        plt.subplot(1, num_filters + 1, 1)
        plt.imshow(original, cmap='gray')
        plt.title("Original")
        plt.axis('off')

        for i, filtered in enumerate(filtered_set[:num_filters]):
            plt.subplot(1, num_filters + 1, i+2)
            plt.imshow(np.array([[filtered]]), cmap='gray')
            plt.title(f"Filter {i+1}")
            plt.axis('off')

        plt.tight_layout()
        plt.show()

def apply_max_pooling(filtered_images):
    len_features = len(filtered_images[0])
    grouping = int(len_features/4)
    max_poold_imeges = []

    for filtered_image in filtered_images:
        max_pooled = []
        indexes = []
        for i in range(0, len_features, grouping):
            pool = filtered_image[i:i+grouping]
            index = np.argmax(pool)
            indexes.append(index)
            max_pooled.append(max(filtered_image[i:i+grouping]))
        max_poold_imeges.append(max_pooled)

    return max_poold_imeges

def add_gaussian_noise(images, mean=0, sigma=0.1):
    noisy_images = []
    for img in images:
        gauss = np.random.normal(mean, sigma, img.shape)
        gauss = gauss.reshape(img.shape)
        noisy_img = img + gauss
        noisy_images.append(np.clip(noisy_img, 0, 255))
    return np.array(noisy_images)
        
def shift_matrix(large_matrix, new_center):
    small_size = 29  # The size of the smaller matrix
    half_small = math.ceil(small_size / 2)
    
    # Calculate the top-left start position for the small matrix
    start_x = new_center[0] - half_small
    start_y = new_center[1] - half_small
    
    # Slice out the new 29x29 matrix from the large matrix
    centered_submatrix = large_matrix[start_y:start_y + small_size, start_x:start_x + small_size]
  
    return centered_submatrix

def get_normal_distributed_center(center, std_dev=1):
    center_x, center_y = center

    # Generate normally distributed offsets with mean 0 and standard deviation std_dev
    offset_x = np.random.normal(0, std_dev)
    offset_y = np.random.normal(0, std_dev)

    # Clip the offsets to be within the range of -2 to 2
    offset_x = np.clip(offset_x, -2, 2)
    offset_y = np.clip(offset_y, -2, 2)

    # Apply the offsets to the center coordinates
    new_center_x = center_x + int(np.round(offset_x))
    new_center_y = center_y + int(np.round(offset_y))

    return (new_center_x, new_center_y)

def pad_pad_select(img):
    img = np.pad(img, ((0, 1), (0, 1)), mode='constant', constant_values=0)
    img = np.pad(img, ((14, 14), (14, 14)), mode='constant', constant_values=0)
    center = (math.ceil(img.shape[0]/2), math.ceil(img.shape[1]/2))
    center = get_normal_distributed_center(center)
    img = shift_matrix(img, center)
    return img

def main():
    mnist_dataset = tf.keras.datasets.mnist
    (train_images, train_labels), (test_images, test_labels) = mnist_dataset.load_data()

    num_samples_per_digit = 500  # Reduser størrelsen for å spare tid under prototyping
    images_by_digit = []
    labels_by_digit = []

    # Samle bilder og labels for hvert siffer
    for digit in range(10):
        digit_images, digit_labels = get_images_of_digit(list(zip(train_images, train_labels)), digit,
                                                         num_samples_per_digit)

        images_by_digit.append(digit_images)
        labels_by_digit.extend([digit] * num_samples_per_digit)

    # Flat ut listen for K-means
    all_images = np.vstack(images_by_digit)
    all_labels = np.array(labels_by_digit)

    kernels = get_gabor_filters()
    # show_gabor_filters(kernels)  # Vis Gabor-filtere hvis ønskelig
    # show_gabor_filters(kernels)

    # all_images_noisy = add_gaussian_noise(all_images)
    all_images_noisy = all_images

    all_images_noisy = [pad_pad_select(img) for img in all_images_noisy]
    # pad images

    # Bruk filterne på alle bildene
    filtered_images = apply_filter(all_images_noisy, kernels)

    filtered_max_pooling = apply_max_pooling(filtered_images)

    # Kjør K-means clustering på de filtrerte bildene
    num_clusters = 42  # Antall sifre
    kmeans = []

    for i in range(0, len(filtered_max_pooling), num_samples_per_digit):
        kmeans.append(kmeans_clustering(filtered_max_pooling[i:i+num_samples_per_digit], num_clusters))

    correct = 0
    total = 0

    # load a random image from the test set
    for index in range(2000):
        img = test_images[index]

        # apply the filters to the image
        filtered_img = apply_filter([img], kernels)
        filtered_maxpooled_img = apply_max_pooling(filtered_img)

        dist, label = -1, 0

        for i in range(len(kmeans)):
            # calculate distance of point to cluster center
            for cluster_center in kmeans[i].cluster_centers_:
                dist_temp = np.linalg.norm(filtered_maxpooled_img[0] - cluster_center)
                if dist_temp < dist or dist == -1:
                    dist = dist_temp
                    label = i

        if label == test_labels[index]:
            correct += 1
        total += 1

    print("Accuracy: ", round(correct / total*100, 2), "%")

if __name__ == "__main__":
    main()