import numpy as np
import torch
import torch.nn as nn
import torchvision
import matplotlib.pyplot as plt


# Load observations from the mnist dataset. The observations are divided into a training set and a test set
mnist_train = torchvision.datasets.MNIST('./data', train=True, download=True)
x_train = mnist_train.data.reshape(-1, 1, 28, 28).float()  # torch.functional.nn.conv2d argument must include channels (1)
y_train = torch.zeros((mnist_train.targets.shape[0], 10))  # Create output tensor
y_train[torch.arange(mnist_train.targets.shape[0]), mnist_train.targets] = 1  # Populate output

mnist_test = torchvision.datasets.MNIST('./data', train=False, download=True)
x_test = mnist_test.data.reshape(-1, 1, 28, 28).float()  # torch.functional.nn.conv2d argument must include channels (1)
y_test = torch.zeros((mnist_test.targets.shape[0], 10))  # Create output tensor
y_test[torch.arange(mnist_test.targets.shape[0]), mnist_test.targets] = 1  # Populate output

# Normalization of inputs
x_train = x_train / 255
x_test = x_test / 255

# Divide training data into batches to speed up optimization
batches = 600
x_train_batches = torch.split(x_train, batches)
y_train_batches = torch.split(y_train, batches)


class ConvolutionalNeuralNetworkModel(nn.Module):

    def __init__(self):
        super(ConvolutionalNeuralNetworkModel, self).__init__()

        # Model layers (includes initialized model variables):
        self.conv1 = nn.Conv2d(1, 32, kernel_size=5, padding=2)
        self.pool1 = nn.MaxPool2d(kernel_size=2)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=5, padding=2)
        self.pool2 = nn.MaxPool2d(kernel_size=2)
        self.dense1 = nn.Linear(64 * 7 * 7, 10)
        self.dense2 = nn.Linear(10, 1024)

    def logits(self, x):
        x = self.conv1(x)
        x = self.pool1(x)
        x = self.conv2(x)
        x = self.pool2(x)
        x = self.dense1(x.reshape(-1, 64 * 7 * 7))
        x = self.dense2(x)
        return x

    # Predictor
    def f(self, x):
        return torch.softmax(self.logits(x), dim=1)

    # Cross Entropy loss
    def loss(self, x, y):
        return nn.functional.cross_entropy(self.logits(x), y.argmax(1))

    # Accuracy
    def accuracy(self, x, y):
        return torch.mean(torch.eq(self.f(x).argmax(1), y.argmax(1)).float())


model = ConvolutionalNeuralNetworkModel()
optimizer = torch.optim.Adam(model.parameters(), 0.001)


def add_gaussian_noise(images, mean=0, sigma=1):
    noisy_images = []
    for img in images:
        gauss = np.random.normal(mean, sigma, img.shape)
        gauss = gauss.reshape(img.shape)
        noisy_img = img + gauss
        noisy_images.append(np.clip(noisy_img, 0, 255))
    return np.array(noisy_images)

def plot_noisy_image(index):
    # Velger et bilde
    original_image = x_test[index].numpy()

    # Legger til støy
    noisy_image = add_gaussian_noise(original_image, mean=0, sigma=1)

    # Plotter originalbildet
    plt.subplot(1, 2, 1)
    plt.imshow(original_image.squeeze(), cmap='gray')
    plt.title('Original Image')
    plt.axis('off')

    # Plotter bildet med støy
    plt.subplot(1, 2, 2)
    plt.imshow(noisy_image.squeeze(), cmap='gray')
    plt.title('Noisy Image')
    plt.axis('off')

    plt.show()


def train_model():
    for epoch in range(20):
        for batch in range(len(x_train_batches)):
            model.loss(x_train_batches[batch], y_train_batches[batch]).backward()
            optimizer.step()
            optimizer.zero_grad()

def runWithoutNoise():
    accuracy_without_noise = model.accuracy(x_test, y_test)
    print("accuracy without noise = %s" % accuracy_without_noise)

def runWithNoise():
    x_test_noisy = torch.tensor(add_gaussian_noise(x_test.numpy(), mean=0, sigma=1), dtype=torch.float32)
    accuracy_with_noise = model.accuracy(x_test_noisy, y_test)
    print("accuracy with noise = %s" % accuracy_with_noise)

def main():
    print("Number of training images:", x_train.shape[0])
    print("Number of testing images:", x_test.shape[0])
    print("Running on sigma = 1")
    train_model()
    runWithoutNoise()
    runWithNoise()
    plot_noisy_image(0)

if __name__ == "__main__":
    main()