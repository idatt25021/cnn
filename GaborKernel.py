import numpy as np
import itertools
import cv2
import math
from sklearn.cluster import KMeans
import tensorflow as tf

class GaborKernel():
  def __init__(self, kernel_size, thetas, lambdas_, sigmas, gammas, psis):
    self.kernel_size = kernel_size
    self.thetas = thetas
    self.lambdas_ = lambdas_
    self.sigmas = sigmas
    self.gammas = gammas
    self.psis = psis


  def get_kernels(self):
    combinations = list(itertools.product(self.kernel_size, self.thetas, self.lambdas_, self.sigmas, self.gammas, self.psis))
    gabor_kernels = [cv2.getGaborKernel((ks, ks), sigma, theta, lambd, gamma, psi, ktype=cv2.CV_32F) for
                     ks, theta, lambd, sigma, gamma, psi in combinations]
    return gabor_kernels
  
  def add_gaussian_noise(self, images, sigma=0.1):
    noisy_images = []
    mean = 0
    for img in images:
        gauss = np.random.normal(mean, sigma, img.shape)
        gauss = gauss.reshape(img.shape)
        noisy_img = img + gauss
        noisy_images.append(noisy_img)
    return np.array(np.clip(noisy_images, 0, 1))

  def apply_gabor_filter(self, images, sigma=0):
    kernels = self.get_kernels()
    vector_kernels = [kernel.flatten() for kernel in kernels]
    normalized_kernels = [np.linalg.norm(vector) for vector in vector_kernels]
    # normalize images
    test_images = images / 255.0
    if(sigma != 0):
      test_images = self.add_gaussian_noise(test_images, sigma=sigma)
    
    # add 1 column and row to each image, below and to the right
    test_images = [np.pad(image, ((0, 1), (0, 1)), mode='constant', constant_values=0) for image in test_images]

    test_images = [image.flatten() for image in test_images]
    normalized_images = [np.linalg.norm(image) for image in test_images]

    # apply filters
    filtered_images = []
    for image, normalized_image in zip(test_images, normalized_images):
        applied_filters = []
        for vector_kernel, normalized_kernel in zip(vector_kernels, normalized_kernels):
          dot_product = np.dot(vector_kernel, image)
          cos_theta = dot_product / (normalized_kernel * normalized_image)
          applied_filters.append(cos_theta)
        filtered_images.append(applied_filters)
    
    return filtered_images
  
  def get_kmeans(self, max_pool_images, num_samples, k):
    num_clusters = k
    kmeans = []

    for i in range(0, len(max_pool_images), num_samples):
        kmeans.append(KMeans(n_clusters=num_clusters, random_state=0, n_init='auto').fit(max_pool_images[i : i + num_samples]))

    return kmeans
  
  def test_images(self, kmeans, test_image_processed, test_labels):
    total = len(test_image_processed)
    correct = 0
    min_dist  = float('inf')
    for image, correct_label in zip(test_image_processed, test_labels):
      min_dist = float('inf')
      label = -1
      for i in range(len(kmeans)):
        for cluster in kmeans[i].cluster_centers_:
          temp_dist = np.linalg.norm(image - cluster)
          if temp_dist < min_dist:
            min_dist = temp_dist
            label = i
      if label == correct_label:
        correct += 1
    
    return correct, total

  def run(self, x_train, y_train, x_test, y_test, sigmas, samples_per_digit):

    print("Training plain Gabor...")
    filtered_images = self.apply_gabor_filter(x_train)
    k_clusters = 56
    kmeans = self.get_kmeans(filtered_images, samples_per_digit, k_clusters)
      

    print("Testing plain Gabor...")
    accuracies = []
    for sigma in sigmas:
      filtered_test_images = self.apply_gabor_filter(x_test, sigma)
      correct, total = self.test_images(kmeans, filtered_test_images, y_test)
      accuracies.append(correct / total )
    
    return accuracies