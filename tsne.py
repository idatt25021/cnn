import cProfile
import itertools
import math
import multiprocessing

import cv2
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE
from matplotlib.colors import ListedColormap



PROCESSES = 10
NUM_OF_DIGITS = 10

def calculate_mesh_points(width=29, height=29, radius=None):
    if radius is None:
        radius = 9

    center_x, center_y = math.ceil(width / 2), math.ceil(height / 2)
    points = [(center_x, center_y)]  # Central point

    for i in range(6):
        angle = 2 * math.pi * i / 6
        x = center_x + radius * math.cos(angle)
        y = center_y + radius * math.sin(angle)
        points.append((round(x), round(y)))

    return points


def find_corresponding_center(m1, m2, center):
    x, y = center
    m2_center = (math.ceil(m2.shape[0] / 2), math.ceil(m2.shape[1] / 2))
    reloc_x = x - math.ceil(m2.shape[0] / 2)
    reloc_y = y - math.ceil(m2.shape[1] / 2)
    img_center = (math.ceil(m1.shape[0] / 2), math.ceil(m1.shape[1] / 2))
    recalc_center = (img_center[0] + reloc_x, img_center[1] + reloc_y)
    x_, y_ = recalc_center
    return (x_, y_)


def get_images_of_digit(dataset, digit, count):
    images = []
    labels = []

    for i in range(len(dataset)):
        image, label = dataset[i]
        if label == digit:
            images.append(np.array(image))
            labels.append(label)
            if len(images) == count:
                break

    return images, labels


def get_gabor_filters():
    kernel_size = np.array([28])
    theta_ = np.arange(0, np.pi, np.pi / 4)
    lambda_ = np.array([3, 5, 7])
    sigma_ = np.array([1.0, 2.0, 3.0])
    gamma_ = np.array([0.5, 1.25, 2.0])
    psi_ = np.array([0, np.pi / 2])

    combinations = list(
        itertools.product(kernel_size, theta_, lambda_, sigma_, gamma_, psi_)
    )

    gabor_kernels = [
        cv2.getGaborKernel((ks, ks), sigma, theta, lambd, gamma, psi, ktype=cv2.CV_32F)
        for ks, theta, lambd, sigma, gamma, psi in combinations
    ]

    return gabor_kernels


def get_all_pixels(center):
    x, y = center
    pixels = []
    for i in range(-2, 3):
        for j in range(-2, 3):
            pixels.append((x + i, y + j))
    return pixels


def apply_filter(images, kernels, centers, return_dict, num):
    norm_kernels = [np.linalg.norm(k.flatten()) for k in kernels]

    normalized_images = []
    for img in images:
        img = np.pad(img, ((0, 1), (0, 1)), mode="constant", constant_values=0)
        img = np.pad(img, ((14, 14), (14, 14)), mode="constant", constant_values=0)
        normalized_images.append(img.astype(np.float32) / 255.0)

    print("Padded and normalized images done")

    kernel_as_vector = []
    for kernel in kernels:
        kernel_as_vector.append(kernel.flatten())

    print("Kernel as vector done")

    filtered_images = []
    amount_of_images = len(normalized_images)
    for center in centers:
        all_pixels = get_all_pixels(center)
        for i, img in enumerate(normalized_images):
            filters_applied = []

            images_shifted = []
            for pixel in all_pixels:
                img_shift_pixel = shift_matrix(img, pixel)
                images_shifted.append(img_shift_pixel)

            for kernel, norm_kernel in zip(kernel_as_vector, norm_kernels):
                # normal distribute center selection and shift the image
                max_values = []
                for shift_image in images_shifted:
                    # flatten out and normalize image
                    image_as_vector = shift_image.flatten()
                    norm_image = np.linalg.norm(image_as_vector)

                    # calculate the dot product and the cos of theta
                    dot_product = np.dot(image_as_vector, kernel)
                    cos_of_theta = dot_product / (norm_image * norm_kernel)
                    # add the filtered value to the list of values
                    max_values.append(cos_of_theta)

                filters_applied.append(max(max_values))

                # add all the filtered values per image
            filtered_images.append(filters_applied)
            if i % 100 == 0:
                print(
                    len(filtered_images) / (amount_of_images * len(centers)) * 100,
                    "% done",
                )
    return_dict[num] = filtered_images


def apply_filter_test(images, kernels, centers):
    norm_kernels = [np.linalg.norm(k.flatten()) for k in kernels]

    normalized_images = []
    for img in images:
        img = np.pad(img, ((0, 1), (0, 1)), mode='constant', constant_values=0)
        img = np.pad(img, ((14, 14), (14, 14)), mode='constant', constant_values=0)
        normalized_images.append(img.astype(np.float32) / 255.0)

    kernel_as_vector = []
    for kernel in kernels:
        kernel_as_vector.append(kernel.flatten())

    filtered_images = []
    for center in centers:
        all_pixels = get_all_pixels(center)
        for img in normalized_images:
            filters_applied = []

            images_shifted = []
            for pixel in all_pixels:
                img_shift_pixel = shift_matrix(img, pixel)
                images_shifted.append(img_shift_pixel)

            for kernel, norm_kernel in zip(kernel_as_vector, norm_kernels):
                # normal distribute center selection and shift the image
                max_values = []
                for shift_image in images_shifted:
                    # flatten out and normalize image
                    image_as_vector = shift_image.flatten()
                    norm_image = np.linalg.norm(image_as_vector)

                    # calculate the dot product and the cos of theta
                    dot_product = np.dot(image_as_vector, kernel)
                    cos_of_theta = dot_product / (norm_image * norm_kernel)
                    # add the filtered value to the list of values
                    max_values.append(cos_of_theta)

                filters_applied.append(max(max_values))

                # add all the filtered values per image
            filtered_images.append(filters_applied)

    return filtered_images


def kmeans_clustering(filtered_images, num_clusters):
    kmeans = KMeans(n_clusters=num_clusters, random_state=42, n_init="auto").fit(
        filtered_images
    )
    return kmeans


def plot_images(original_images, filtered_images, kernels, num_filters=5):
    for idx, (original, filtered_set) in enumerate(
            zip(original_images, filtered_images)
    ):
        plt.figure(figsize=(15, 3))
        plt.subplot(1, num_filters + 1, 1)
        plt.imshow(original, cmap="gray")
        plt.title("Original")
        plt.axis("off")

        for i, filtered in enumerate(filtered_set[:num_filters]):
            plt.subplot(1, num_filters + 1, i + 2)
            plt.imshow(np.array([[filtered]]), cmap="gray")
            plt.title(f"Filter {i + 1}")
            plt.axis("off")

        plt.tight_layout()
        plt.show()


def apply_max_pooling(filtered_images):
    len_features = len(filtered_images[0])
    grouping = int(len_features / 4)
    max_poold_images = []

    for filtered_image in filtered_images:
        max_pooled = []
        for i in range(0, len_features, grouping):
            max_pooled.append(max(filtered_image[i: i + grouping]))
        max_poold_images.append(max_pooled)

    return max_poold_images


def add_gaussian_noise(images, mean=0, sigma=0.1):
    noisy_images = []
    for img in images:
        gauss = np.random.normal(mean, sigma, img.shape)
        gauss = gauss.reshape(img.shape)
        noisy_img = img + gauss
        noisy_images.append(np.clip(noisy_img, 0, 255))
    return np.array(noisy_images)


def shift_matrix(large_matrix, new_center):
    small_size = 29  # The size of the smaller matrix
    half_small = math.ceil(small_size / 2)

    # Calculate the top-left start position for the small matrix
    start_x = new_center[0] - half_small
    start_y = new_center[1] - half_small

    # Slice out the new 29x29 matrix from the large matrix
    centered_submatrix = large_matrix[
                         start_y: start_y + small_size, start_x: start_x + small_size
                         ]

    return centered_submatrix


def get_normal_distributed_center(center, std_dev=1):
    center_x, center_y = center

    # Generate normally distributed offsets with mean 0 and standard deviation std_dev
    offset_x = np.random.normal(0, std_dev)
    offset_y = np.random.normal(0, std_dev)

    # Clip the offsets to be within the range of -2 to 2
    offset_x = np.clip(offset_x, -2, 2)
    offset_y = np.clip(offset_y, -2, 2)

    # Apply the offsets to the center coordinates
    new_center_x = center_x + int(np.round(offset_x))
    new_center_y = center_y + int(np.round(offset_y))

    return (new_center_x, new_center_y)


def pad_pad_select(img, center):
    img = np.pad(img, ((0, 1), (0, 1)), mode="constant", constant_values=0)
    img = np.pad(img, ((14, 14), (14, 14)), mode="constant", constant_values=0)
    calc_center = get_normal_distributed_center(center)
    img = shift_matrix(img, calc_center)
    return img


def main():
    mnist_dataset = tf.keras.datasets.mnist
    (train_images, train_labels), (test_images, test_labels) = mnist_dataset.load_data()

    num_samples_per_digit = 10  # Reduser størrelsen for å spare tid under prototyping
    images_by_digit = []
    labels_by_digit = []
    print("Loading images...")
    # Samle bilder og labels for hvert siffer
    for digit in range(10):
        digit_images, digit_labels = get_images_of_digit(
            list(zip(train_images, train_labels)), digit, num_samples_per_digit
        )

        images_by_digit.append(digit_images)
        labels_by_digit.extend([digit] * num_samples_per_digit)

    # Flat ut listen for K-means
    all_images = np.vstack(images_by_digit)
    all_labels = np.array(labels_by_digit)

    kernels = get_gabor_filters()
    # show_gabor_filters(kernels)  # Vis Gabor-filtere hvis ønskelig
    # show_gabor_filters(kernels)

    # Find honeycomb centers and the corresponding ones
    centers = calculate_mesh_points()
    matrix_29 = np.zeros((29, 29))
    matrix_57 = np.zeros((57, 57))
    centers = [
        find_corresponding_center(matrix_57, matrix_29, center) for center in centers
    ]
    print("Applying filters...")
    # Bruk filterne på alle bildene
    splited_images = np.array_split(all_images, PROCESSES)
    splitted_labels = np.array_split(all_labels, PROCESSES)
    manager = multiprocessing.Manager()
    return_dict = manager.dict()

    threads = []
    for i, image_chunk in enumerate(splited_images):
        num = all_labels[i * len(image_chunk)]
        p = multiprocessing.Process(
            target=apply_filter, args=(image_chunk, kernels, centers, return_dict, num)
        )
        p.start()
        threads.append(p)

    for thread in threads:
        thread.join()

    filtered_images = []
    for i in range(10):
        filtered_images.extend(return_dict[i])

    clusters = 7
    filtered_list = [[] for i in range(clusters)]
    num_per_digit = num_samples_per_digit
    for digit in range(10):
        for cluster in range(clusters):
            start_index = digit * clusters * num_per_digit + cluster * num_per_digit
            filtered_list[cluster].append(filtered_images[start_index:start_index + num_per_digit])

    for i in range(7):
        filtered_list[i] = np.array(filtered_list[i])

    filtered_list = [np.reshape(arr, (num_per_digit * 10, 216)) for arr in filtered_list]
    filtered_images = np.concatenate(filtered_list, axis=0)

    filtered_max_pooling = []
    print("Applying max pooling...")
    for i in range(0, len(filtered_images), num_samples_per_digit * 10):
        filtered_max_pooling.append(
            apply_max_pooling(filtered_images[i: i + num_samples_per_digit * 10])
        )

    # Convert to numpy array
    filtered_max_pooling = np.array(filtered_max_pooling)

    # combine the features from r4 => r28
    combined_features = np.concatenate(filtered_max_pooling, axis=-1)

    # Kjør K-means clustering på de filtrerte bildene
    num_clusters = 2  # Antall sifre
    kmeans = []
    print("Clustering...")
    for i in range(0, len(combined_features), num_samples_per_digit):
        kmeans.append(
            kmeans_clustering(
                combined_features[i: i + num_samples_per_digit], num_clusters
            )
        )

    print(kmeans[0].cluster_centers_)

    all_kluster_centeres = []

    for centers in kmeans:
        for center in centers.cluster_centers_:
            all_kluster_centeres.append(center)

    all_kluster_centeres = np.array(all_kluster_centeres)

    X_embedded = TSNE(n_components=2, perplexity=5, random_state=42).fit_transform(all_kluster_centeres)

    my_colors = ['#e6194b', '#3cb44b', '#ffe119', '#4363d8', '#f58231',
                 '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#fabebe']

    # Create a custom colormap
    custom_cmap = ListedColormap(my_colors[:NUM_OF_DIGITS])

    # Assuming each cluster has an equal number of centers
    points_per_cluster = len(all_kluster_centeres) // NUM_OF_DIGITS

    plt.figure(figsize=(8, 6))
    for i in range(NUM_OF_DIGITS):
        # Calculate start and end indices for each cluster's centers
        start_idx = i * points_per_cluster
        end_idx = start_idx + points_per_cluster

        # Select the data points belonging to the current cluster
        cluster_points = X_embedded[start_idx:end_idx]

        # Scatter plot for each cluster in a different color
        plt.scatter(cluster_points[:, 0], cluster_points[:, 1], color=custom_cmap(i), label=f'Cluster {i + 1}')

    # Adding titles and labels
    plt.title('t-SNE of Combined Features')
    plt.xlabel('Component 1')
    plt.ylabel('Component 2')
    plt.legend()  # Add a legend to distinguish clusters

    # Show the plot
    plt.show()

if __name__ == "__main__":
    main()
