import cv2
import numpy as np
import tensorflow as tf
import itertools
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.manifold import TSNE

NUMBERS_OF_THETA = 4
TESTS = 50
TRAINING_SIZE = 500

def get_images():
    mnist = tf.keras.datasets.mnist
    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()
    return train_images

def get_images_of_digit(dataset, digit, count):
    images = []
    labels = []

    for i in range(len(dataset)):
        image, label = dataset[i]
        if label == digit:
            images.append(np.array(image))
            labels.append(label)
            if len(images) == count:
                break

    return images, labels

def get_gabor_filters():
    kernel_size = np.array([28])
    theta_ = np.arange(0, np.pi, np.pi / NUMBERS_OF_THETA)
    lambda_ = np.array([3, 5, 7])
    sigma_ = np.array([1.0, 2.0, 3.0])
    gamma_ = np.array([0.5, 1.25, 2.0])
    psi_ = np.array([0, np.pi / 2])

    combinations = list(itertools.product(kernel_size, theta_, lambda_, sigma_, gamma_, psi_))

    gabor_kernels = [cv2.getGaborKernel((ks, ks), sigma, theta, lambd, gamma, psi, ktype=cv2.CV_32F) for
                     ks, theta, lambd, sigma, gamma, psi in combinations]

    return gabor_kernels


def apply_filter(images, kernels):
    kernel_size = kernels[0].shape
    norm_kernels = [np.linalg.norm(k.flatten()) for k in kernels]  # Pre-compute norms filtered_images = []

    filtered_images = []
    for img in images:

        img_float = img.astype(np.float32) / 255.0
        img_size = img_float.shape

        if kernel_size != img_size:
            padding = (
                (0, kernel_size[0] - img_size[0]),
                (0, kernel_size[1] - img_size[1])
            )
            img_float_padded = np.pad(img_float, padding, mode='constant', constant_values=0)
        else:
            img_float_padded = img_float

        if kernels[0].shape != img_float_padded.shape:
            raise ValueError("Kernel and image must have the same dimensions after padding")

        image_as_vector = img_float_padded.flatten()
        norm_image = np.linalg.norm(image_as_vector)

        filters_applied = []

        for kernel, norm_kernel in zip(kernels, norm_kernels):

            kernel_as_vector = kernel.flatten()
            dot_product = np.dot(image_as_vector, kernel_as_vector)
            cos_of_theta = dot_product / (norm_image * norm_kernel)

            filters_applied.append(cos_of_theta)

        filtered_images.append(filters_applied)

    return filtered_images


def kmeans_clustering(filtered_images, num_clusters):
    kmeans = KMeans(n_clusters=num_clusters, random_state=42, n_init='auto').fit(filtered_images)
    return kmeans


def show_gabor_filters(kernels):
    n_filters = len(kernels)
    n_cols = 4
    n_rows = (n_filters + n_cols - 1) // n_cols
    fig, axs = plt.subplots(nrows=n_rows, ncols=n_cols, figsize=(20, 2 * n_rows))
    axs = axs.flatten()
    for ax, kernel in zip(axs, kernels):
        ax.imshow(kernel, cmap='gray')
        ax.axis('off')
    plt.show()

def plot_images(original_images, filtered_images, kernels, num_filters=5):
    for idx, (original, filtered_set) in enumerate(zip(original_images, filtered_images)):
        plt.figure(figsize=(15, 3))
        plt.subplot(1, num_filters + 1, 1)
        plt.imshow(original, cmap='gray')
        plt.title("Original")
        plt.axis('off')

        for i, filtered in enumerate(filtered_set[:num_filters]):
            plt.subplot(1, num_filters + 1, i+2)
            plt.imshow(np.array([[filtered]]), cmap='gray')
            plt.title(f"Filter {i+1}")
            plt.axis('off')

        plt.tight_layout()
        plt.show()

def apply_max_pooling(filtered_images):
    len_features = len(filtered_images[0])
    grouping = int(len_features/NUMBERS_OF_THETA)
    max_poold_imeges = []

    for filtered_image in filtered_images:
        max_pooled = []
        indexes = []
        for i in range(0, len_features, grouping):
            pool = filtered_image[i:i+grouping]
            index = np.argmax(pool)
            indexes.append(index)
            max_pooled.append(max(filtered_image[i:i+grouping]))
        max_poold_imeges.append(max_pooled)

    return max_poold_imeges

def print_accuracy(kernels, kmeans, test_images, test_labels):
    correct = 0
    total = 0
    # load a random image from the test set
    for index in range(TESTS):
        img = test_images[index]

        # apply the filters to the image
        filtered_img = apply_filter([img], kernels)
        filtered_maxpooled_img = apply_max_pooling(filtered_img)

        dist, label = -1, 0

        for i in range(len(kmeans)):
            # calculate distance of point to cluster center
            for cluster_center in kmeans[i].cluster_centers_:
                dist_temp = np.linalg.norm(filtered_maxpooled_img[0] - cluster_center)
                if dist_temp < dist or dist == -1:
                    dist = dist_temp
                    label = i

        if label == test_labels[index]:
            correct += 1
        total += 1
    print("Accuracy: ", round(correct / total * 100, 2), "%")


def plot_dataset(dataset):
    length_og_dataset = int(len(dataset)/10)
    tsne = TSNE(n_components=2, learning_rate='auto', init='random', perplexity=3)
    plt.figure(figsize=(10, 8))

    colors = plt.cm.rainbow(np.linspace(0, 1, len(dataset)))

    for i in range(0, len(dataset), length_og_dataset):
        # Perform TSNE on each dataset
        data = np.array(dataset[i:i+length_og_dataset])
        X_embedded = tsne.fit_transform(data)

        # Scatter plot for each dataset with unique color
        plt.scatter(X_embedded[:, 0], X_embedded[:, 1], color=colors[i], label=f'Dataset {i + 1}')

    plt.title('2D TSNE-Plot of 10 Datasets')
    plt.xlabel('Component 1')
    plt.ylabel('Component 2')
    plt.legend()  # Show legend
    plt.colorbar()  # Shows color scale
    plt.show()


def main():
    mnist_dataset = tf.keras.datasets.mnist
    (train_images, train_labels), (test_images, test_labels) = mnist_dataset.load_data()

    num_samples_per_digit = TRAINING_SIZE  # Reduser størrelsen for å spare tid under prototyping
    images_by_digit = []
    labels_by_digit = []

    # Samle bilder og labels for hvert siffer
    for digit in range(10):
        digit_images, digit_labels = get_images_of_digit(list(zip(train_images, train_labels)), digit,
                                                         num_samples_per_digit)

        images_by_digit.append(digit_images)
        labels_by_digit.extend([digit] * num_samples_per_digit)

    # Flat ut listen for K-means
    all_images = np.vstack(images_by_digit)
    all_labels = np.array(labels_by_digit)

    kernels = get_gabor_filters()
    # show_gabor_filters(kernels)  # Vis Gabor-filtere hvis ønskelig
    # show_gabor_filters(kernels)
    # Bruk filterne på alle bildene
    filtered_images = apply_filter(all_images, kernels)
    filtered_max_pooling = apply_max_pooling(filtered_images)


    # Kjør K-means clustering på de filtrerte bildene
    num_clusters = 42  # Antall sifre
    kmeans = []

    for i in range(0, len(filtered_max_pooling), num_samples_per_digit):
        kmeans.append(kmeans_clustering(filtered_max_pooling[i:i+num_samples_per_digit], num_clusters))

    print_accuracy(kernels, kmeans, test_images, test_labels)


    plot_dataset(filtered_max_pooling)


if __name__ == "__main__":
    main()