import numpy as np
import itertools
import cv2
import math
from sklearn.cluster import KMeans
import multiprocessing
import tensorflow as tf

class GaborKernelMesh():
  def __init__(self, kernel_size, thetas, lambdas_, sigmas, gammas, psis):
    self.kernel_size = kernel_size
    self.thetas = thetas
    self.lambdas_ = lambdas_
    self.sigmas = sigmas
    self.gammas = gammas
    self.psis = psis


  def get_kernels(self):
    combinations = list(itertools.product(self.kernel_size, self.thetas, self.lambdas_, self.sigmas, self.gammas, self.psis))
    gabor_kernels = [cv2.getGaborKernel((ks, ks), sigma, theta, lambd, gamma, psi, ktype=cv2.CV_32F) for
                            ks, theta, lambd, sigma, gamma, psi in combinations]    
    return gabor_kernels
  
  def get_images_of_digit(self, dataset, digit, count):
    images = []
    labels = []

    for i in range(len(dataset)):
        image, label = dataset[i]
        if label == digit:
            images.append(np.array(image))
            labels.append(label)
            if len(images) == count:
                break

    return images, labels
    
  def add_gaussian_noise(self, images, sigma=0.1):
    noisy_images = []
    mean = 0
    for img in images:
        gauss = np.random.normal(mean, sigma, img.shape)
        gauss = gauss.reshape(img.shape)
        noisy_img = img + gauss
        noisy_images.append(np.clip(noisy_img, 0, 255))
    return np.array(noisy_images)
  
  def find_corresponding_center(self, m1, m2, center):
    x, y = center
    m2_center = (math.ceil(m2.shape[0] / 2), math.ceil(m2.shape[1] / 2))
    reloc_x = x - math.ceil(m2.shape[0] / 2)
    reloc_y = y - math.ceil(m2.shape[1] / 2)
    img_center = (math.ceil(m1.shape[0] / 2), math.ceil(m1.shape[1] / 2))
    recalc_center = (img_center[0] + reloc_x, img_center[1] + reloc_y)
    x_, y_ = recalc_center
    return (x_, y_)
  
  def get_filter_centers(self):
    radius = 9
    height, width = 29, 29
    center_x, center_y = math.ceil(width / 2), math.ceil(height / 2)
    points = [(center_x, center_y)]  # Central point

    for i in range(6):
        angle = 2 * math.pi * i / 6
        x = center_x + radius * math.cos(angle)
        y = center_y + radius * math.sin(angle)
        points.append((round(x), round(y)))
    centers = points
    matrix_29 = np.zeros((29, 29))
    matrix_57 = np.zeros((57, 57))
    centers = [self.find_corresponding_center(matrix_57, matrix_29, center) for center in centers]
    return centers
  
  def get_all_pixels(self, center):
    x, y = center
    pixels = []
    for i in range(-2, 3):
      for j in range(-2, 3):
        pixels.append((x+i, y+j))
    return pixels
  
  def shift_matrix(self, large_matrix, new_center):
    small_size = 29  # The size of the smaller matrix
    half_small = math.ceil(small_size / 2)

    # Calculate the top-left start position for the small matrix
    start_x = new_center[0] - half_small
    start_y = new_center[1] - half_small

    # Slice out the new 29x29 matrix from the large matrix
    centered_submatrix = large_matrix[
        start_y : start_y + small_size, start_x : start_x + small_size
    ]

    return centered_submatrix  
  
  def apply_gabor_filter_training(self, images):
    # get and prepare kernels
    kernels = self.get_kernels()
    vector_kernels = [kernel.flatten() for kernel in kernels]
    normalized_kernels = [np.linalg.norm(vector) for vector in vector_kernels]

    # normalize images
    normalized_images = images / 255.0

    # pad images 
    padded_images = [np.pad(norm_img, ((14, 15), (14, 15)), mode='constant', constant_values=0) for norm_img in normalized_images]

    # get centers
    centers = self.get_filter_centers()

    # apply filters
    filtered_images = []
    for center in centers:
      shifted_images = [self.shift_matrix(img, center).flatten() for img in padded_images]
      normalized_shifted_images = [np.linalg.norm(shifted_image) for shifted_image in shifted_images]
      for shifted_image, normalized_shifted_image in zip(shifted_images, normalized_shifted_images):
        applied_filters = []
        for vector_kernel, normalized_kernel in zip(vector_kernels, normalized_kernels):
          dot_product = np.dot(vector_kernel, shifted_image)
          cos_theta = dot_product / (normalized_kernel * normalized_shifted_image)
          applied_filters.append(cos_theta)
        filtered_images.append(applied_filters)
    
    return filtered_images

  def apply_gabor_filter_testing(self, images, sigma=0):
    # get and prepare kernels
    filter_kernels = self.get_kernels()
    vector_kernels = [kernel.flatten() for kernel in filter_kernels]
    normalized_kernels = [np.linalg.norm(vector) for vector in vector_kernels]

    # normalize images
    normalized_images = images / 255.0

    # add specified noise
    if(sigma != 0):
      normalized_images = self.add_gaussian_noise(normalized_images, sigma=sigma)

    # pad images 
    padded_images = [np.pad(norm_img, ((14, 15), (14, 15)), mode='constant', constant_values=0) for norm_img in normalized_images]

    # get centers
    centers = self.get_filter_centers()

    # apply filters
    filtered_images = []
    for center in centers:
      shifted_images = [self.shift_matrix(img, center).flatten() for img in padded_images]
      normalized_shifted_images = [np.linalg.norm(shifted_image) for shifted_image in shifted_images]
      for shifted_image, normalized_shifted_image in zip(shifted_images, normalized_shifted_images):
        applied_filters = []
        for vector_kernel, normalized_kernel in zip(vector_kernels, normalized_kernels):
          dot_product = np.dot(vector_kernel, shifted_image)
          cos_theta = dot_product / (normalized_kernel * normalized_shifted_image)
          applied_filters.append(cos_theta)
        filtered_images.append(applied_filters)
    return filtered_images
  
  def max_pool(self, filtered_image):
    len_features = len(filtered_image)
    grouping = int(len_features / 4)
    max_pooled = []
    for i in range(0, len_features, grouping):
      max_pooled.append(max(filtered_image[i : i + grouping]))
    return max_pooled
  
  def apply_max_pooling(self, filtered_images):
    len_features = len(filtered_images[0])
    grouping = int(len_features / 4)
    max_poold_images = []

    for filtered_image in filtered_images:
        max_pooled = []
        for i in range(0, len_features, grouping):
            max_val = max(filtered_image[i:i+grouping])
            max_pooled.append(max_val)
        max_poold_images.append(max_pooled)

    return max_poold_images
  
  def apply_max_pooling_train(self, filtered_images, samples_per_digit):
    max_pool = []
    for i in range(0, len(filtered_images), samples_per_digit*10):
      max_pool.append(self.apply_max_pooling(filtered_images[i : i + samples_per_digit*10]))
    # Convert to numpy array
    max_pool = np.array(max_pool)
    # combine the features from r4 => r28
    combined_features = np.concatenate(max_pool, axis=-1)
    return combined_features

  def apply_max_pooling_test(self, filtered_images):
    max_pool = []
    for i in range(len(filtered_images)):
      max_pool.append(self.max_pool(filtered_images[i]))
    test_max_pool = np.array(max_pool)
    test_max_pool = np.array_split(test_max_pool, 7)
    test_combined_features = np.concatenate(test_max_pool, axis=-1)
    return test_combined_features
  
  def get_kmeans(self, max_pool_images, num_samples, k):
    num_clusters = k
    kmeans = []

    for i in range(0, len(max_pool_images), num_samples):
        kmeans_ = KMeans(n_clusters=num_clusters, random_state=42, n_init='auto').fit(max_pool_images[i : i + num_samples])
        kmeans.append(kmeans_)

    return kmeans
  
  def test_images(self, kmeans, test_image_processed, test_labels):
    correct = 0
    for image, correct_label in zip(test_image_processed, test_labels):
      min_dist  = float('inf')
      label = -1
      for i in range(len(kmeans)):
        for cluster in kmeans[i].cluster_centers_:
          temp_dist = np.linalg.norm(image - cluster)
          if temp_dist < min_dist:
            min_dist = temp_dist
            label = i
      if label == correct_label:
        correct += 1
    
    return correct

  def run(self, x_train, y_train, x_test, y_test, sigmas, samples_per_digit):
    print("Training Gabor with mesh...")
    filtered_images = self.apply_gabor_filter_training(x_train)

    max_pool = self.apply_max_pooling_train(filtered_images, samples_per_digit)
    k_clusters = 66
    kmeans_result = self.get_kmeans(max_pool, samples_per_digit, k_clusters)

    print("Testing Gabor with mesh...")
    accuracies = []
    for sigma in sigmas:
      test_images_processed = self.apply_gabor_filter_testing(x_test, sigma)
      test_max_pool = self.apply_max_pooling_test(test_images_processed)
      correct = self.test_images(kmeans_result, test_max_pool, y_test)
      accuracies.append(correct / len(y_test))
    
    return accuracies
    
    
